import React from "react";

import "./Register.css"

class Register extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            username: '',
            gender: '',
            age: null,
            type: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        //setting the state of multiple input elements
        let inputName = event.target.name;
        let inputValue = event.target.value;
        this.setState({[inputName]: inputValue});
    }
    
    handleSubmit(event) {
        alert('Submitted: ' + this.state.username + " " + this.state.gender + " " + this.state.age + " " + this.state.type);
        event.preventDefault();
    }

    render() {
        let femaleAdditionalLabel = '';

        //Conditional Rendering
        if (this.state.gender === "female") {
            femaleAdditionalLabel = <div>
                <label>Female additional:</label><br></br>
            </div>;
        }

        return(
            <div>
                <h1>Register please</h1>
                <form className="registration-form" onSubmit={this.handleSubmit}>
                    <label>Name:</label>
                    <input type="text" name="username" value={this.state.username} onChange={this.handleChange} placeholder="Please enter your name" required/>
                    <br></br>

                    <label>Gender:</label>
                    <select name="gender" onChange={this.handleChange}>
                        <option selected value="default"></option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                    <br></br>

                    {femaleAdditionalLabel}

                    <label>Age:</label>
                    <input type="number" name="age" value={this.state.age} onChange={this.handleChange} placeholder="Please enter your age" required/>
                    <br></br>

                    <label>Type:</label>
                    Private
                    <input type="radio" id="radioPrivate" name="type" value="private" onChange={this.handleChange}/>
                    Public
                    <input type="radio" id="radioPublic" name="type" value="public" onChange={this.handleChange}/>
                    <br></br>
                    <input type="submit" value="Submit"/>
                </form>
            </div>
        )
    }
}

export default Register;