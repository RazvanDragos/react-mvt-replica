import React from "react";

import "./Videos.css";

class Videos extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            pageName: "Videos",
            videos: [ //the list of entities fetched from backend
                {id: 1, name: "Learn React", price: "200", length: "120 min"},
                {id: 2, name: "Learn Angular", price: "180", length: "130 min"},
                {id: 3, name: "Learn Java", price: "300", length: "160 min"},
                {id: 4, name: "Learn C#", price: "270", length: "140 min"}
            ]
        }
    }

    render() {
        //loop through all entities and create a table row element for each
        const videos = this.state.videos.map( video => (
            <tr key={video.id}>
                <td>{video.name}</td>
                <td>{video.price * 2}</td>
                <td>{video.length}</td>
            </tr>
        ));

        const videosCount = videos.length;
        
        return (
            <div>
                <h1>The {this.state.pageName} Page</h1>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Length</th>
                        </tr>
                    </thead>
                        
                    <tbody>
                        {videos}
                    </tbody>
                </table>
                <h2>Total number of videos: {videosCount}</h2>
            </div>
        );
    }
}

export default Videos;