import React from 'react';
import FeaturedChampion from '../FeaturedChampion/FeaturedChampion';
import Header from '../header/Header';

class LeagueOfLegends extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allChampions: []
        };
    }

    //equivalent to 'ngAfterViewInit()' from Angular. It is called when a component is Mounted
    componentDidMount() {
        this.fetchChampions();
    }

    //Acts just like a Service method which fetches data from APIs
    fetchChampions = () => {
        fetch('champions.json')
            .then(response => response.json())
            .then((champions) => {
                this.setState({ allChampions: champions })
            });
    }

    render() {
        return (
            <div>
                <Header subtitle="This is the League of Legends official website"/>
                <FeaturedChampion champions={this.state.allChampions} />
            </div>
        )
    }
}

export default LeagueOfLegends;