import React from 'react';

import { Route, Switch, Redirect } from "react-router-dom";
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';

import Courses from "../../containers/Courses/Courses";
import Users from "../../containers/Users/Users";
import NoMatch from "../NoMatch/NoMatch";
import Course from "../../containers/Course/Course";
import Videos from "../../containers/videos/Videos";
import Register from "../../containers/register/Register";
import LeagueOfLegends from "..//LeagueOfLegends/LeagueOfLegends";
import DataSources from "../DataSources/DataSources";
import SideNavigationBar from "../SideNavigationBar/SideNavigationBar";
import Test from "../Main/Test";

class Main extends React.Component {
    render() {
        return (
            <div className="">
                <SideNavigationBar />
                {/* <Test /> */}
                <Switch>
                    <Route path="/courses/:courseID" component={Course} />
                    <Route path="/courses" component={Courses} />
                    <Route path="/users" component={Users} />
                    <Route path="/videos" component={Videos} />
                    <Route path="/register" component={Register} />
                    <Route path="/leagueoflegends" component={LeagueOfLegends} />
                    <Route path="/datasources" component={DataSources} />
                    <Redirect from="/all-courses" to="/courses" />
                    <Route component={NoMatch} />
                </Switch>
                {/* <div className="container">
                    <TopNavigationBar />
                    <LeftNavigationBar />
                    <LeftNavigationTree/>
                </div> */}
                
        </div>
        )
    }
}

export default Main;