import React from 'react';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import "./DataSources.css";

class DataSources extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSources: []
        }
    }

    componentDidMount() {
        this.fetchDataSources();
    }

    fetchDataSources = () => {
        fetch('/data-source')
            .then(response => response.json())
            .then((dataSources) => {
                this.setState({ dataSources: dataSources.content })
            })
    }

    render() {
        return (
            <div>
                <TableContainer component={Paper} className="table-container">
                    <div>
                        <input placeholder='Search for..'/>
                    </div>
                    <Table aria-label="simple table" className="table-contents">
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell align="right">Description</TableCell>
                                <TableCell align="right">Type</TableCell>
                                <TableCell align="right">Creation Date</TableCell>
                                <TableCell align="right">Modify Date</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.dataSources.map((dataSource) => (
                                <TableRow key={dataSource.id}>
                                <TableCell component="th" scope="row">
                                    {dataSource.name}
                                </TableCell>
                                <TableCell align="right">{dataSource.description}</TableCell>
                                <TableCell align="right">{dataSource.type}</TableCell>
                                <TableCell align="right">{dataSource.creationDate}</TableCell>
                                <TableCell align="right">{dataSource.modifiedDate}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        );
    };
}

export default DataSources;