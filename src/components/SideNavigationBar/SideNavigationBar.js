import React from 'react';

import { NavLink } from "react-router-dom";
import { NavBarData } from "../NavBarData/NavBarData";
import * as FaIcons from "react-icons/fa";

import "./SideNavigationBar.css"

class SideNavigationBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sidebar: true,
        }
    }

    showSideBar = () => {
        this.setState({ sidebar: !this.state.sidebar })
    }

    render() {
        
        return (
            <div>
                <div className='navbar'>
                    <NavLink to='#' className='menu-bars'>
                        <FaIcons.FaBars onClick={this.showSideBar}/>
                    </NavLink>
                </div>
                <nav className={this.state.sidebar ? 'nav-menu active' : 'nav-menu'}>
                    <ul className='nav-menu-items'>
                        {NavBarData.map((item, index) => {
                        return(
                            <li key={index} className={item.className}>
                            <NavLink to={item.path}>
                                <span>{item.icon}</span>
                                <span className='nav-titles'>{item.title}</span>
                            </NavLink>
                            </li>
                        )
                        })}
                    </ul>
                </nav>
            </div>
        );
    }
}

export default SideNavigationBar;