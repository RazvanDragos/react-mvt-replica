import React from 'react';
import Champion from '../Champion/Champion';

import "./FeaturedChampion.css";

class FeaturedChampion extends React.Component {
    state = {
        value: "",
    }

    handleFilter = (e) => {
        this.setState({
            value: e.target.value           
        });
    };

    filterChampions = () => {
        return this.props.champions.filter(champion => {
            return champion.name.toLowerCase().includes(this.state.value.toLowerCase())
        })
    }

    render() {
        const searchBox = (
            <input
                type="text"
                //onChange={this.filterChampions}
                onChange={this.handleFilter}
            />
        );

        const filteredData = this.filterChampions().map(champ => 
            <Champion key={champ.id} champion = {champ}/>
        );

        if (this.props.champions) {
            return (
                <div>
                    <div>
                        Filter champions by name: {searchBox}
                    </div>
                    <div className="champion-container">
                        {filteredData}
                    </div> 
                </div>  
            );
        }
        return (<div>No featured champion available</div>) 
    }
}

export default FeaturedChampion;