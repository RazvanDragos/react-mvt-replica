import React from 'react';
import "./Champion.css"

class Champion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        const champion = this.props.champion;
        return(
                <div className="statistics-grid">
                    <div>
                        <img className="champion-image" src={champion.image} alt=""></img>
                    </div>
                    <div className="statistics-detailed">
                        <div>Name: {champion.name}</div>
                        <div>Role: {champion.role}</div>
                        <div>Price: {champion.price}</div>  
                    </div>
                    {/* <div className="abilities-video">
                        <div><iframe src={champion.abilitiesVideo}></iframe></div>
                    </div> */}
                </div> 
        );
    }
}

export default Champion;