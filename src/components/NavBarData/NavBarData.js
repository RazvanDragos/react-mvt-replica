import React from 'react';
import * as BsIcons from "react-icons/bs";
import * as AiIcons from "react-icons/ai";
import * as FiIcons from "react-icons/fi";
import * as HiIcons from "react-icons/hi";

export const NavBarData = [
    {
        title: 'Dashboard',
        path: '/courses',
        icon: <AiIcons.AiOutlineDashboard />,
        className: "nav-text"
    },
    {
        title: 'DataSources',
        path: '/datasources',
        icon: <BsIcons.BsCollection />,
        className: "nav-text"
    },
    {
        title: 'DataSets',
        path: '/users',
        icon: <BsIcons.BsCollection />,
        className: "nav-text"
    },
    {
        title: 'Jobs',
        path: '/videos',
        icon: <BsIcons.BsCollection />,
        className: "nav-text"
    },
    {
        title: 'Configuration',
        path: '/register',
        icon: <FiIcons.FiSettings />,
        className: "nav-text"
    },
    {
        title: 'Reports',
        path: '/leagueoflegends',
        icon: <HiIcons.HiOutlineDocumentReport />,
        className: "nav-text"
    }
];